package com.yubest.fs.service.impl;

import com.yubest.fs.bean.*;
import com.yubest.fs.config.FileStorageProperties;
import com.yubest.fs.consts.Consts;
import com.yubest.fs.exception.StorageException;
import com.yubest.fs.util.FileUtil;
import io.minio.*;
import io.minio.http.Method;
import lombok.Data;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @Author hweiyu
 * @Description
 * @Date 2022/2/9 17:09
 */
public class MinioStorageImpl extends AbstractStorage<MinioStorageImpl.InnerMinioClient> {

    private static final Logger log = LoggerFactory.getLogger(MinioStorageImpl.class);

    @Override
    public void prepareClient(String bucket) {
        //校验配置信息是否完整
        if (null == getProps() || null == getProps().getMinio()) {
            throw new StorageException("minio configuration is missing");
        }
        FileStorageProperties.Config config = this.getProps().getMinio();
        List<FileStorageProperties.Bucket> buckets = config.getBuckets();
        if (CollectionUtils.isEmpty(buckets)) {
            throw new StorageException("minio buckets configuration is missing");
        }
        if (StringUtils.isEmpty(config.getAccessKey()) || StringUtils.isEmpty(config.getAccessSecret())) {
            throw new StorageException("minio accesKey, accessSecret configuration is missing");
        }
        //选择使用的桶，如果没传入，则使用默认的桶
        String useBucket = StringUtils.isEmpty(bucket) ? config.getPrimaryBucket() : bucket;
        FileStorageProperties.Bucket bucketInfo;
        //如果没指定桶，则使用第一个
        if (null == useBucket) {
            bucketInfo = buckets.get(0);
        } else {
            bucketInfo = buckets.stream()
                    .filter(e -> Objects.equals(useBucket, e.getBucket()))
                    .findFirst()
                    .orElseThrow(() -> new StorageException("minio bucket:[" + useBucket + "] is missing"));
        }
        if (null == bucketInfo.getEndPoint()) {
            throw new StorageException("minio, fetch end point error");
        }
        try {
            //创建客户端
            MinioClient minioClient = MinioClient.builder()
                    .endpoint(bucketInfo.getEndPoint())
                    .credentials(config.getAccessKey(), config.getAccessSecret())
                    .build();
            setStorageClient(new InnerMinioClient()
                    .setBucketInfo(bucketInfo)
                    .setClient(minioClient));
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("minio client create error", e);
            }
            throw new StorageException("minio client create error");
        }
    }

    @Override
    public PutResponse upload(PutFileRequest request) {
        PutResponse uploader = new PutResponse();

        MinioClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();

        String path = getPath(myBucket.getPrefix(), request.getFileName());

        try(ByteArrayInputStream is = new ByteArrayInputStream(request.getPayload())) {
            //封装metadata，设置用户自定义meta
            FileMeta fileMeta = new FileMeta()
                    .addAll(request.getMeta())
                    .add(Consts.META_FILE_NAME, !StringUtils.isEmpty(request.getFileName()) ? request.getFileName() : "")
                    .add(Consts.META_FILE_CONTENT_TYPE, !StringUtils.isEmpty(request.getContentType()) ? request.getContentType() : "")
                    .add(Consts.META_FILE_SIZE, String.valueOf(request.getPayload().length));

            myClient.putObject(PutObjectArgs.builder()
                    .bucket(myBucket.getBucket())
                    .object(path)
                    .stream(is, request.getPayload().length, -1)
                    .userMetadata(fileMeta)
                    .build());
            uploader.setPath(path);
            uploader.setUrl(this.getAccessUrl(new GetFileRequest().setBucket(request.getBucket()).setPath(path)));
        } catch (Exception e){
            if (log.isErrorEnabled()) {
                log.error("upload file error, filePath:{}", path, e);
            }
            uploader.setCode(Consts.CODE_ERROR);
            uploader.setError(e);
        }
        return uploader;
    }

    @Override
    public FileStream download(GetFileRequest request) {
        MinioClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();
        GetObjectResponse res = null;
        try(ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            res = myClient.getObject(GetObjectArgs.builder()
                    .bucket(myBucket.getBucket())
                    .object(request.getPath())
                    .build());
            StreamUtils.copy(res, os);
            FileMeta meta = doGetMeta(request);
            return new FileStream(meta, os.toByteArray());
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("download file error, filePath:{}", request.getPath(), e);
            }
            throw new StorageException("download file error, file path:" + request.getPath());
        } finally {
            FileUtil.close(res);
        }
    }

    @Override
    public String getAccessUrl(GetFileRequest request) {
        MinioClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();
        try {
            String rawUrl = myBucket.getDomain() + "/" + request.getPath();
            String url;
            //如果是私有的桶，封装时效性访问url
            if (Consts.PRIVATE_STORAGE.equals(myBucket.getType())) {
                url = myClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                        .method(Method.GET)
                        .bucket(myBucket.getBucket())
                        .object(request.getPath())
                        .expiry(myBucket.getExpiresTime().intValue(), TimeUnit.SECONDS)
                        .build());
            } else {
                url = rawUrl;
            }
            return url;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("fetch file access path error, file path:{}", request.getPath(), e);
            }
            throw new StorageException("fetch file access path error, file path:" + request.getPath());
        }
    }

    @Override
    public FileMeta doGetMeta(GetFileRequest request) {
        MinioClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();
        try {
            StatObjectResponse response = myClient.statObject(StatObjectArgs.builder()
                    .bucket(myBucket.getBucket())
                    .object(request.getPath())
                    .build());
            return new FileMeta().addAll(response.userMetadata());
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("delete file error, file path:{}", request.getPath(), e);
            }
            throw new StorageException("delete file error, file path:" + request.getPath());
        }
    }

    @Override
    public boolean isFileExists(GetFileRequest request) {
        MinioClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();
        try {
            myClient.statObject(StatObjectArgs.builder()
                    .bucket(myBucket.getBucket())
                    .object(request.getPath())
                    .build());
            return true;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("delete file error, file path:{}", request.getPath(), e);
            }
        }
        return false;
    }

    @Override
    public boolean delete(RemoveFileRequest request) {
        MinioClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();
        try {
            myClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(myBucket.getBucket())
                    .object(request.getPath())
                    .build());
            return true;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("delete file error, file path:{}", request.getPath(), e);
            }
            throw new StorageException("delete file error, file path:" + request.getPath());
        }
    }

    @Data
    @Accessors(chain = true)
    static class InnerMinioClient {

        private MinioClient client;

        private FileStorageProperties.Bucket bucketInfo;
    }
}
