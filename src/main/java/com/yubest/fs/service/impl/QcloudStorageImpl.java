package com.yubest.fs.service.impl;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import com.yubest.fs.bean.*;
import com.yubest.fs.config.FileStorageProperties;
import com.yubest.fs.consts.Consts;
import com.yubest.fs.exception.StorageException;
import com.yubest.fs.util.FileUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Author hweiyu
 * @Description
 * @Date 2022/2/9 17:59
 */
public class QcloudStorageImpl extends AbstractStorage<QcloudStorageImpl.QcloudClient> {

    private static final Logger log = LoggerFactory.getLogger(QcloudStorageImpl.class);

    @Override
    public void prepareClient(String bucket) {
        //校验配置信息是否完整
        if (null == getProps() || null == getProps().getQcloud()) {
            throw new StorageException("qcloud configuration is missing");
        }
        FileStorageProperties.Config config = this.getProps().getQcloud();
        List<FileStorageProperties.RegionBucket> buckets = config.getBuckets();
        if (CollectionUtils.isEmpty(buckets)) {
            throw new StorageException("qcloud buckets configuration is missing");
        }
        if (StringUtils.isEmpty(config.getAccessKey()) || StringUtils.isEmpty(config.getAccessSecret())) {
            throw new StorageException("qcloud accesKey, accessSecret configuration is missing");
        }
        //选择使用的桶，如果没传入，则使用默认的桶
        String useBucket = StringUtils.isEmpty(bucket) ? config.getPrimaryBucket() : bucket;
        FileStorageProperties.RegionBucket bucketInfo;
        //如果没指定桶，则使用第一个
        if (null == useBucket) {
            bucketInfo = buckets.get(0);
        } else {
            bucketInfo = buckets.stream()
                    .filter(e -> Objects.equals(useBucket, e.getBucket()))
                    .findFirst()
                    .orElseThrow(() -> new StorageException("qcloud bucket:[" + useBucket + "] is missing"));
        }
        if (null == bucketInfo.getRegion()) {
            throw new StorageException("qcloud, fetch region error");
        }
        ClientConfig clientConfig = new ClientConfig();
        //设置bucket所在的区域，华南：gz 华北：tj 华东：sh
        clientConfig.setRegion(new Region(bucketInfo.getRegion()));
        //创建认证对象
        COSCredentials credentials = new BasicCOSCredentials(config.getAccessKey(), config.getAccessSecret());
        //创建客户端
        QcloudClient qcloudClient = new QcloudClient()
                .setBucketInfo(bucketInfo)
                .setClient(new COSClient(credentials, clientConfig));
        setStorageClient(qcloudClient);
    }

    @Override
    public void closeClient() {
        //关闭客户端
        getStorageClient().getClient().shutdown();
        clearStorageClient();
    }

    @Override
    public PutResponse upload(PutFileRequest request) {
        PutResponse uploader = new PutResponse();

        COSClient myClient = getStorageClient().getClient();
        FileStorageProperties.RegionBucket myBucket = getStorageClient().getBucketInfo();

        String path = getPath(myBucket.getPrefix(), request.getFileName());
        try {
            //上传到腾讯云
            ObjectMetadata metadata = new ObjectMetadata();
            //封装metadata，设置用户自定义meta
            FileMeta fileMeta = new FileMeta()
                    .addAll(request.getMeta())
                    .add(Consts.META_FILE_NAME, !StringUtils.isEmpty(request.getFileName()) ? request.getFileName() : "")
                    .add(Consts.META_FILE_CONTENT_TYPE, !StringUtils.isEmpty(request.getContentType()) ? request.getContentType() : "")
                    .add(Consts.META_FILE_SIZE, String.valueOf(request.getPayload().length));
            metadata.setUserMetadata(fileMeta);
            PutObjectRequest putRequest = new PutObjectRequest(
                    myBucket.getBucket(), "/" + path, new ByteArrayInputStream(request.getPayload()), metadata);
            myClient.putObject(putRequest);
            uploader.setPath(path);
            uploader.setUrl(this.getAccessUrl(new GetFileRequest().setBucket(request.getBucket()).setPath(path)));
        } catch (Exception e){
            if (log.isErrorEnabled()) {
                log.error("upload file error, filePath:{}", path, e);
            }
            uploader.setCode(Consts.CODE_ERROR);
            uploader.setError(e);
        }
        return uploader;
    }

    @Override
    public FileStream download(GetFileRequest request) {
        COSClient myClient = getStorageClient().getClient();
        FileStorageProperties.RegionBucket myBucket = getStorageClient().getBucketInfo();
        COSObjectInputStream is = null;
        try(ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            COSObject cOSObject = myClient.getObject(myBucket.getBucket(), request.getPath());
            is = cOSObject.getObjectContent();
            StreamUtils.copy(is, os);
            FileMeta meta = doGetMeta(request);
            return new FileStream(meta, os.toByteArray());
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("download file error, filePath:{}", request.getPath(), e);
            }
            throw new StorageException("download file error, file path:" + request.getPath());
        } finally {
            FileUtil.close(is);
        }
    }

    @Override
    public String getAccessUrl(GetFileRequest request) {
        COSClient myClient = getStorageClient().getClient();
        FileStorageProperties.RegionBucket myBucket = getStorageClient().getBucketInfo();
        try {
            String rawUrl = myBucket.getDomain() + "/" + request.getPath();
            String url;
            //如果是私有的桶，封装时效性访问url
            if (Consts.PRIVATE_STORAGE.equals(myBucket.getType())) {
                GeneratePresignedUrlRequest urlRequest =
                        new GeneratePresignedUrlRequest(myBucket.getBucket(), request.getPath());
                urlRequest.setExpiration(new Date(new Date().getTime() + myBucket.getExpiresTime() * 1000));
                URL objectUrl = myClient.generatePresignedUrl(urlRequest);
                url = objectUrl.toString();
            } else {
                url = rawUrl;
            }
            return url;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("fetch file access path error, file path:{}", request.getPath(), e);
            }
            throw new StorageException("fetch file access path error, file path:" + request.getPath());
        }
    }

    @Override
    public FileMeta doGetMeta(GetFileRequest request) {
        COSClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();
        try {
            ObjectMetadata meta = myClient.getObjectMetadata(myBucket.getBucket(), request.getPath());
            return new FileMeta().addAll(meta.getUserMetadata());
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("fetch file metadata error, filePath:{}", request.getPath(), e);
            }
            throw new StorageException("fetch file metadata error, file path:" + request.getPath());
        }
    }

    @Override
    public boolean isFileExists(GetFileRequest request) {
        COSClient myClient = getStorageClient().getClient();
        FileStorageProperties.Bucket myBucket = getStorageClient().getBucketInfo();
        try {
            return myClient.doesObjectExist(myBucket.getBucket(), request.getPath());
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("does file exist error, filePath:{}", request.getPath(), e);
            }
            throw new StorageException("does file exist error, file path:" + request.getPath());
        }
    }

    @Override
    public boolean delete(RemoveFileRequest request) {
        COSClient myClient = getStorageClient().getClient();
        FileStorageProperties.RegionBucket myBucket = getStorageClient().getBucketInfo();
        try {
            myClient.deleteObject(myBucket.getBucket(), request.getPath());
            return true;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("delete file error, file path:{}", request.getPath(), e);
            }
            throw new StorageException("delete file error, file path:" + request.getPath());
        }
    }

    @Data
    @Accessors(chain = true)
    static class QcloudClient {

        private COSClient client;

        private FileStorageProperties.RegionBucket bucketInfo;
    }
}
