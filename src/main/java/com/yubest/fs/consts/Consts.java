package com.yubest.fs.consts;

/**
 * @author huangweiyu
 * @version V1.0
 * @date 21:53
 **/
public class Consts {

    /**
     * 自定义文件处理器，key值
     */
    public static final String CUSTOM_STORAGE_CLASS = "storage-class";

    /**
     * 请求成功状态码
     */
    public static final int CODE_SUCCESS = 0;

    /**
     * 请求失败状态码
     */
    public static final int CODE_ERROR = 1000;

    /**
     * 默认文件content type
     */
    public static final String STREAM_CONTENT_TYPE = "application/octet-stream";

    /**
     * 1天，过期时间
     */
    public static final long EXPIRES_TIME = 24 * 3600;

    /**
     * 桶，私有类型
     */
    public static final String PRIVATE_STORAGE = "private";

    /**
     * meta常量
     */
    public static final String LOCAL_META_FILE_SUFFIX = ".meta";

    public static final String LOCAL_META_USER_VAR_PREFIX = "u-";

    public static final String META_FILE_NAME = "--g--file-name";

    public static final String META_FILE_CONTENT_TYPE = "--g--file-content-type";

    public static final String META_FILE_SIZE = "--g--file-size";

}
