package com.yubest.fs;

import com.yubest.fs.bean.*;
import com.yubest.fs.consts.Consts;

/**
 * 文件存储统一接口
 * @Author hweiyu
 * @Description
 * @Date 2022/2/9 12:05
 */
public interface FileStorage {

    /**
     * 上传
     * @param fileName
     * @param bytes
     * @return
     */
    default PutResponse put(String fileName, byte[] bytes) {
        return put(fileName, Consts.STREAM_CONTENT_TYPE, bytes);
    }

    default PutResponse put(String fileName, String contentType, byte[] payload) {
        return put(null, fileName, contentType, payload);
    }

    default PutResponse put(String bucket, String fileName, String contentType, byte[] payload) {
        return put(bucket, fileName, contentType, payload, null);
    }

    PutResponse put(String bucket, String fileName, String contentType, byte[] payload, FileMeta meta);

    /**
     * 下载
     * @param filePath
     * @return
     */
    default FileStream get(String filePath) {
        return get(null, filePath);
    }

    FileStream get(String bucket, String filePath);

    /**
     * 获取文件metadata
     * @param filePath
     * @return
     */
    default FileMeta getMeta(String filePath) {
        return getMeta(null, filePath);
    }

    FileMeta getMeta(String bucket, String filePath);

    /**
     * 获取访问路径
     * @param filePath
     * @return
     */
    default String getUrl(String filePath) {
        return getUrl(null, filePath);
    }

    String getUrl(String bucket, String filePath);

    /**
     * 判断文件是否存在
     * @param filePath
     * @return
     */
    default boolean isExists(String filePath) {
        return isExists(null, filePath);
    }

    boolean isExists(String bucket, String filePath);

    /**
     * 删除
     * @param filePath
     * @return
     */
    default boolean remove(String filePath) {
        return remove(null, filePath);
    }

    boolean remove(String bucket, String filePath);

    /**
     * 使用阿里云oss处理器
     * @return
     */
    FileStorage useAliyun();

    /**
     * 使用七牛云oss处理器
     * @return
     */
    FileStorage useQiniu();

    /**
     * 使用腾讯云oss处理器
     * @return
     */
    FileStorage useQcloud();

    /**
     * 使用minio oss处理器
     * @return
     */
    FileStorage useMinio();

    /**
     * 使用本地存储处理器
     * @return
     */
    FileStorage useLocal();

    /**
     * 使用自定义oss处理器
     * @return
     */
    FileStorage useCustom();

    void setPrimaryStorage(String primaryStorage);

}
