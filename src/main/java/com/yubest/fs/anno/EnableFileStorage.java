package com.yubest.fs.anno;

import com.yubest.fs.config.FileStorageConfigurationSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启自动装配功能
 * 与 META-INF/spring.factories 功能一致，存在一个即可
 * @Author hweiyu
 * @Description
 * @Date 2022/1/27 17:19
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({FileStorageConfigurationSelector.class})
public @interface EnableFileStorage {
}
