package com.yubest.fs.enums;

import com.yubest.fs.service.impl.*;

/**
 * @Author hweiyu
 * @Description
 * @Date 2022/2/9 11:45
 */
public enum StorageTypeEnum {

    //阿里oss
    ALIYUN(AliyunStorageImpl.class.getName(), "com.aliyun.oss.OSS"),

    //七牛oss
    QINIU(QiniuStorageImpl.class.getName(), "com.qiniu.storage.UploadManager"),

    //腾讯云oss
    QCLOUD(QcloudStorageImpl.class.getName(), "com.qcloud.cos.COSClient"),

    //minio fs
    MINIO(MinioStorageImpl.class.getName(), "io.minio.MinioClient"),

    //本地文件存储
    LOCAL(LocalStorageImpl.class.getName(), LocalStorageImpl.class.getName()),

    //自定义oss
    CUSTOM(Void.class.getName(), Void.class.getName()),

    ;

    private String loaderClass;

    private String conditionClass;

    StorageTypeEnum(String loaderClass, String conditionClass) {
        this.loaderClass = loaderClass;
        this.conditionClass = conditionClass;
    }

    public String getLoaderClass() {
        return loaderClass;
    }

    public String getConditionClass() {
        return conditionClass;
    }

    public static StorageTypeEnum of(String name) {
        if (null != name) {
            for (StorageTypeEnum type : values()) {
                if (type.name().equalsIgnoreCase(name)) {
                    return type;
                }
            }
        }
        return null;
    }

}
