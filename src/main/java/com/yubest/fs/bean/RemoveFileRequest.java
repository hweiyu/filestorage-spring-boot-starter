package com.yubest.fs.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 删除文件，请求参数
 * @Author hweiyu
 * @Description
 * @Date 2022/2/14 9:09
 */
@Data
@Accessors(chain = true)
public class RemoveFileRequest {

    /**
     * 桶
     */
    private String bucket;

    /**
     * 存储路径
     */
    private String path;
}
