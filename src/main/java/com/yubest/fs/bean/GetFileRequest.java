package com.yubest.fs.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 获取文件，请求参数
 * @Author hweiyu
 * @Description
 * @Date 2022/2/14 9:09
 */
@Data
@Accessors(chain = true)
public class GetFileRequest {

    /**
     * 桶
     */
    private String bucket;

    /**
     * 存储路径
     */
    private String path;
}
