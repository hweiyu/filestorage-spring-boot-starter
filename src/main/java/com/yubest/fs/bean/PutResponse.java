package com.yubest.fs.bean;

import com.yubest.fs.consts.Consts;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 上传文件，返回结果
 * @Author hweiyu
 * @Description
 * @Date 2022/2/9 14:03
 */
@Data
@Accessors(chain = true)
public class PutResponse {

    private String path;

    private String url;

    private Integer code = Consts.CODE_SUCCESS;

    private Exception error;

    public boolean isSuccess() {
        return null != getCode() && getCode() == Consts.CODE_SUCCESS;
    }

}
