package com.yubest.fs.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件metadata
 * @Author hweiyu
 * @Description
 * @Date 2022/2/11 9:48
 */
public class FileMeta extends HashMap<String, String> {

    public FileMeta add(String key, String value) {
        this.put(key, value);
        return this;
    }

    public FileMeta addAll(Map<String, String> map) {
        if (null != map) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                this.put(entry.getKey(), entry.getValue());
            }
        }
        return this;
    }
}
