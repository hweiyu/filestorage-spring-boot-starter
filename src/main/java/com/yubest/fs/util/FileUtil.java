package com.yubest.fs.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;

/**
 * 文件处理类
 * @Author hweiyu
 * @Description
 * @Date 2022/2/9 14:30
 */
public final class FileUtil {

    private static final Logger log = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 通过文件名获取后缀
     * @param fileName
     * @return
     */
    public static String getExt(String fileName) {
        if (null == fileName) {
            return null;
        }
        int index = fileName.lastIndexOf(".");
        return index > 0 ? fileName.substring(index + 1) : null;
    }

    /**
     * 关闭流
     * @param s
     */
    public static boolean close(Closeable s) {
        if (null == s) {
            return true;
        }
        try {
            s.close();
            return true;
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("close stream error", e);
            }
        }
        return false;
    }

}
