package com.yubest.fs.config;

import com.yubest.fs.FileStorage;
import com.yubest.fs.bean.FileStorageBuilder;
import com.yubest.fs.service.impl.AbstractStorage;
import com.yubest.fs.service.impl.FileStorageImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * 自动装配注入bean
 * @Author hweiyu
 * @Description
 * @Date 2022/1/26 16:54
 */
@Configuration
@EnableConfigurationProperties(FileStorageProperties.class)
public class FileStorageAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(name = "fileStorage")
    public FileStorage fileStorage(FileStorageProperties props) {
        Map<String, AbstractStorage> storageMap = new FileStorageBuilder().setProps(props).build();
        return new FileStorageImpl(props.getPrimary().toLowerCase(), storageMap);
    }

}
