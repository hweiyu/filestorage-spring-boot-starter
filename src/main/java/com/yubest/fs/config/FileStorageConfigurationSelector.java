package com.yubest.fs.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 自动装配选择器
 * @Author hweiyu
 * @Description
 * @Date 2022/1/27 17:30
 */
public class FileStorageConfigurationSelector implements ImportSelector {

    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{FileStorageAutoConfiguration.class.getName()};
    }
}
