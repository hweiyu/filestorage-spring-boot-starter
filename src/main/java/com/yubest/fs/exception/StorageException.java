package com.yubest.fs.exception;

/**
 * 自定义异常类
 * @Author hweiyu
 * @Description
 * @Date 2022/2/9 16:57
 */
public class StorageException extends RuntimeException {

    public StorageException() {
        super();
    }

    public StorageException(String message) {
        super(message);
    }
}
