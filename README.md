# 介绍
* `filestorage-spring-boot-starter` 集成多家OSS云存储，阿里、七牛、腾讯云等等，通过统一接口，进行文件的管理。

# 使用教程
### 1、maven 引入
```maven
    <dependencies>
        <dependency>
            <groupId>com.gitee.hweiyu</groupId>
            <artifactId>filestorage-spring-boot-starter</artifactId>
            <version>1.0.0-beta.1</version>
        </dependency>
    <dependencies>
	
	<!--jitpack仓库-->
    <repositories>
        <repository>
            <id>jitpack.io</id>
            <url>https://www.jitpack.io</url>
        </repository>
    </repositories>
```
### 2、启动类中添加 `@EnableFileStorage` 注解，开启文件存储功能   

### 3、配置oss   
* 可参考`config.example`进行配置
```properties
filestorage:
  # 默认使用本地存储，存储类型：aliyun, qiniu, qcloud, minio, local, custom
  primary: local
  # 阿里云oss
  aliyun:
    # 密钥 key
    accessKey: xxx
    # 密钥
    accessSecret: xxx
    # 默认桶
    primaryBucket: defaultBucket
    # 桶列表
    buckets:
        # 桶名称
      - bucket: defaultBucket
        # 桶类型：公共 public, 私有：private
        type: private
        # 域名
        domain: 'https://www.xxx.com'
        # 端点
        endPoint: xxx
        # 前缀
        prefix: xxx
        # 过期时间，单位/秒
        expires: 86400
      - bucket: myBucket
        type: public
        domain: 'https://www.xxx.com'
        endPoint: xxx
        prefix: xxx
        expires: 86400
  # 七牛云oss
  qiniu:
    accessKey: xxx
    accessSecret: xxx
    # 默认桶
    primaryBucket: defaultBucket
    # 桶列表
    buckets:
        # 桶名称
      - bucket: defaultBucket
        # 桶类型：公共 public, 私有：private
        type: private
        # 域名
        domain: 'https://www.xxx.com'
        # 前缀
        prefix: xxx
        # 有效期
        expires: 86400
        # 地区{huadong, huabei, huanan, beimei, xinjiapo}
        region: huanan
      - bucket: myBucket
        type: public
        domain: 'https://www.xxx.com'
        prefix: xxx
        expires: 86400
        region: huanan
  # 腾讯云oss
  qcloud:
    accessKey: xxx
    accessSecret: xxx
    # 默认桶
    primaryBucket: defaultBucket
    # 桶列表
    buckets:
        # 桶名称
      - bucket: defaultBucket
        # 桶类型：公共 public, 私有：private
        type: private
        # 域名
        domain: 'https://www.xxx.com'
        # 前缀
        prefix: xxx
        # 有效期
        expires: 86400
        # 地区简称，https://cloud.tencent.com/document/product/436/6224
        region: xxx
      - bucket: myBucket
        type: public
        domain: 'https://www.xxx.com'
        prefix: xxx
        expires: 86400
        region: xxx
  # minio oss
  minio:
    # 密钥 key
    accessKey: xxx
    # 密钥
    accessSecret: xxx
    # 默认桶
    primaryBucket: defaultBucket
    # 桶列表
    buckets:
        # 桶名称
      - bucket: defaultBucket
        # 域名
        domain: http://localhost:9000
        # 端点
        endPoint: http://localhost:9000
        # 前缀
        prefix: xxx
        # 有效期
        expires: 86400
      - bucket: myBucket
        domain: http://localhost:9000
        endPoint: http://localhost:9000
        prefix: xxx
        expires: 86400
  # 本地存储
  local:
    # 存储路径
    dir: D:/workspace/hwy/upload
    # 域名
    domain: http://localhost:8080/view
    # 前缀
    prefix:
  # 扩展信息，比例自定义存储，需要实现 com.yubest.fs.service.impl.AbstractStorage 类
  ext:
    # 自定义文件存储器
    storage-class: com.xxx.MyCustomStorageImpl

```

### 4、使用
```java
    @Autowired
    private FileStorage fileStorage;

    @PostMapping(value = "/upload")
    public Object upload(@RequestPart MultipartFile file) {
        try {
            return fileStorage.put(file.getOriginalFilename(), file.getBytes());
        } catch (IOException e) {
            return "{\"code\":-1, \"msg\":\"error\"}";
        }
    }
```
